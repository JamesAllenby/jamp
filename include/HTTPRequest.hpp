#ifndef HTTPREQUEST_HPP
#define HTTPREQUEST_HPP

#include <string>
#include <vector>
#include <map>

#include "HTTPCommon.hpp"

namespace JAMP
{
  class HTTPRequest
  {
  private:
    JAMP::Method m_method{};
    std::string m_uri{};
    std::string m_http_version{};

    // Header field key-value pairs
    std::multimap<std::string, std::string> m_fields;

    // Message
    std::string m_message;
  public:
    HTTPRequest(std::string);

    // Private member variable accessors
    const auto& Method()       { return m_method; }       /* Returns the request method        */
    const auto& URI()          { return m_uri; }          /* Returns the request URI           */
    const auto& HTTPVersion()  { return m_http_version; } /* Returns the request HTTP version  */
    const auto& HeaderFields() { return m_fields; }       /* Returns the request header fields */
    const auto& Message()      { return m_message; }      /* Returns the request message       */

    std::string Serialize();
    void Deserialize(const std::string);
  };
}

#endif // HTTPREQUEST_HH
