#ifndef URIRESOLVER_HPP
#define URIRESOLVER_HPP

#include <map>
#include <regex>

#include "HTTPCommon.hpp"
#include "HTTPRequest.hpp"
#include "HTTPResponse.hpp"

typedef std::function<void (JAMP::HTTPRequest&, JAMP::HTTPResponse&)> HTTPCallback;
typedef std::map<std::string, HTTPCallback> UriMap;

class URIResolver
{
private:
  std::map<JAMP::Method, UriMap*> m_method_map;
public:
  URIResolver();
  HTTPCallback Resolve(JAMP::HTTPRequest);

  template<JAMP::Method T>
  void Register(std::string uri, HTTPCallback callback)
  {
    m_method_map.at(T)->emplace(uri, callback);
  }

  void RegisterAll(std::string uri, HTTPCallback callback)
  {
    Register<JAMP::Method::GET>(uri, callback);
    Register<JAMP::Method::PUT>(uri, callback);
    Register<JAMP::Method::HEAD>(uri, callback);
    Register<JAMP::Method::POST>(uri, callback);
    Register<JAMP::Method::TRACE>(uri, callback);
    Register<JAMP::Method::DELETE>(uri, callback);
    Register<JAMP::Method::CONNECT>(uri, callback);
    Register<JAMP::Method::OPTIONS>(uri, callback);
  }
};

#endif // URIRESOLVER_HPP
