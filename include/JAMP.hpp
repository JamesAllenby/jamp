﻿#ifndef JAMP_HPP
#define JAMP_HPP

#include <string>
#include <functional>

#include "HTTPRequest.hpp"
#include "HTTPResponse.hpp"
#include "URIResolver.hpp"

constexpr int MAX_CLIENT_MESSAGE = 8192;

typedef std::function<void (JAMP::HTTPRequest&, JAMP::HTTPResponse&)> HTTPCallback;

namespace JAMP
{
  class Server
  {
  private:
    int m_sock;
    URIResolver m_uri_resolver;
  public:
    Server();

    // Register functions
    void All(std::string, HTTPCallback);
    void Get(std::string, HTTPCallback);
    void Put(std::string, HTTPCallback);
    void Head(std::string, HTTPCallback);
    void Post(std::string, HTTPCallback);
    void Trace(std::string, HTTPCallback);
    void Delete(std::string, HTTPCallback);
    void Connect(std::string, HTTPCallback);
    void Options(std::string, HTTPCallback);

    [[noreturn]] void Start(ushort port);
};
}

#endif // JAMPSERVER_HH
