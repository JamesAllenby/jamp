#ifndef HTTPRESPONSE_HPP
#define HTTPRESPONSE_HPP

#include <string>
#include <sstream>

#include "HTTPCommon.hpp"

namespace JAMP
{
  class HTTPResponse
  {
  private:
    // Connection file descriptor to write to
    int m_connection_fd;
    // Response code of current response
    JAMP::Code m_response_code;
    // Map of all headers to send to client
    std::multimap<std::string, std::string> m_fields;
    // Message to send to client
    std::string m_message;
    //


  public:
    explicit HTTPResponse(int connection_fd);

    void SetResponseCode(JAMP::Code response_code);

    void AddHeader(const std::string&, const std::string&);

    void Write(std::string message_body);

    void Close();

    const std::string Serialize();
  };
}

#endif // HTTPRESPONSE_H
