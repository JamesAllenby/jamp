#ifndef HTTPCOMMON_HPP
#define HTTPCOMMON_HPP

#include <map>

typedef unsigned short ushort;
typedef std::multimap<std::string, std::string> HeaderFields;

static std::string CRLF = "\r\n";
static std::string SP = " ";

namespace JAMP
{
  enum class Method {
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    CONNECT,
    OPTIONS,
    TRACE
  };

  // RFC7231 Section 8.2
  enum class Code {
    // Informational 1xx
    HTTP_100, // 100 Continue
    HTTP_101, // 101 Switching Protocols

    // Successful 2xx
    HTTP_200, // 200 OK
    HTTP_201, // 201 Created
    HTTP_202, // 202 Accepted
    HTTP_203, // 203 Non-Authoritative Information
    HTTP_204, // 204 No Content
    HTTP_205, // 205 Reset Content

    // Redirection 3xx
    HTTP_300, // 300 Multiple Choices
    HTTP_301, // 301 Moved Permanently
    HTTP_302, // 302 Found
    HTTP_303, // 303 See Other
    HTTP_305, // 305 Use Proxy
    HTTP_306, // 306 (Unused)
    HTTP_307, // 307 Temporary Redirect

    // Client Error 4xx
    HTTP_400, // 400 Bad Request
    HTTP_402, // 402 Payment Required
    HTTP_403, // 403 Forbidden
    HTTP_404, // 404 Not Found
    HTTP_405, // 405 Method Not Allowed
    HTTP_406, // 406 Not Acceptable
    HTTP_408, // 408 Request Timeout
    HTTP_409, // 409 Conflict
    HTTP_410, // 410 Gone
    HTTP_411, // 411 Length Required
    HTTP_413, // 413 Payload Too Large
    HTTP_414, // 414 URI Too Long
    HTTP_415, // 415 Unsupported Media Type
    HTTP_417, // 417 Expectation Failed
    HTTP_426, // 426 Upgrade Required

    // Server Error 5xx
    HTTP_500, // 500 Internal Server Error
    HTTP_501, // 501 Not Implemented
    HTTP_502, // 502 Bad Gateway
    HTTP_503, // 503 Service Unavailable
    HTTP_504, // 504 Gateway Timeout
    HTTP_505, // 505 HTTP Version Not Supported
  };

  const static std::map<JAMP::Code, std::string> StringCode = {
    { Code::HTTP_100, "100" },
    { Code::HTTP_101, "101" },

    { Code::HTTP_200, "200" },
    { Code::HTTP_201, "201" },
    { Code::HTTP_202, "202" },
    { Code::HTTP_203, "203" },
    { Code::HTTP_204, "204" },
    { Code::HTTP_205, "205" },

    { Code::HTTP_300, "300" },
    { Code::HTTP_301, "301" },
    { Code::HTTP_302, "302" },
    { Code::HTTP_303, "303" },
    { Code::HTTP_305, "305" },
    { Code::HTTP_306, "306" },
    { Code::HTTP_307, "307" },

    { Code::HTTP_400, "400" },
    { Code::HTTP_402, "402" },
    { Code::HTTP_403, "403" },
    { Code::HTTP_404, "404" },
    { Code::HTTP_405, "405" },
    { Code::HTTP_406, "406" },
    { Code::HTTP_408, "408" },
    { Code::HTTP_409, "409" },
    { Code::HTTP_410, "410" },
    { Code::HTTP_411, "411" },
    { Code::HTTP_413, "413" },
    { Code::HTTP_414, "414" },
    { Code::HTTP_415, "415" },
    { Code::HTTP_417, "417" },
    { Code::HTTP_426, "426" },

    { Code::HTTP_500, "500" },
    { Code::HTTP_501, "501" },
    { Code::HTTP_502, "502" },
    { Code::HTTP_503, "503" },
    { Code::HTTP_504, "504" },
    { Code::HTTP_505, "505" },
  };

  const static std::map<JAMP::Code, std::string> ReasonPhrase = {
    { Code::HTTP_100, "Continue" },
    { Code::HTTP_101, "Switching Protocols" },

    { Code::HTTP_200, "OK" },
    { Code::HTTP_201, "Created" },
    { Code::HTTP_202, "Accepted" },
    { Code::HTTP_203, "Non-Authoritative Information" },
    { Code::HTTP_204, "No Content" },
    { Code::HTTP_205, "Reset Content" },

    { Code::HTTP_300, "Multiple Choices" },
    { Code::HTTP_301, "Moved Permanently" },
    { Code::HTTP_302, "Found" },
    { Code::HTTP_303, "See Other" },
    { Code::HTTP_305, "Use Proxy" },
    { Code::HTTP_306, "(Unused)" },
    { Code::HTTP_307, "Temporary Redirect" },

    { Code::HTTP_400, "Bad Request" },
    { Code::HTTP_402, "Payment Required" },
    { Code::HTTP_403, "Forbidden" },
    { Code::HTTP_404, "Not Found" },
    { Code::HTTP_405, "Method Not Allowed" },
    { Code::HTTP_406, "Not Acceptable" },
    { Code::HTTP_408, "Request Timeout" },
    { Code::HTTP_409, "Conflict" },
    { Code::HTTP_410, "Gone" },
    { Code::HTTP_411, "Length Required" },
    { Code::HTTP_413, "Payload Too Large" },
    { Code::HTTP_414, "URI Too Long" },
    { Code::HTTP_415, "Unsupported Media Type" },
    { Code::HTTP_417, "Expectation Failed" },
    { Code::HTTP_426, "Upgrade Required" },

    { Code::HTTP_500, "Internal Server Error" },
    { Code::HTTP_501, "Not Implemented" },
    { Code::HTTP_502, "Bad Gateway" },
    { Code::HTTP_503, "Service Unavailable" },
    { Code::HTTP_504, "Gateway Timeout" },
    { Code::HTTP_505, "HTTP Version Not Supported" },
  };
}

#endif // HTTPCOMMON_H
