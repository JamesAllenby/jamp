cmake_minimum_required(VERSION 2.8)

project(JAMP)

set(CMAKE_CXX_STANDARD "17")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wpedantic -Werror")

include_directories("include")

add_library(${PROJECT_NAME} "JAMP.cpp" "HTTPRequest.cpp" "HTTPResponse.cpp" "URIResolver.cpp")
add_executable("WebServer" "main.cc")

target_link_libraries("WebServer" ${PROJECT_NAME})
