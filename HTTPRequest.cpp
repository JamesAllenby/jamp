#include "HTTPRequest.hpp"

#include <iostream>
#include <algorithm>

JAMP::HTTPRequest::HTTPRequest(std::string request)
{
  Deserialize(request);
}

void JAMP::HTTPRequest::Deserialize(std::string request)
{
  // Storage before committing changes
  JAMP::Method method;
  std::string uri;
  std::string http_version;
  std::multimap<std::string, std::string> fields;
  std::string message;

  // Find the position of where the header ends,
  // if it doesn't then throw an error.
  const size_t header_end = request.find("\r\n\r\n");
  if (header_end == std::string::npos)
    throw "Invalid HTTP request";

  // Find the length of the start line and make sure
  // it exists before continuing to process.
  const size_t start_line_length = request.find(CRLF);
  if (start_line_length == std::string::npos)
    throw "Unable to find start line";

  { // Start of start line processing

    // Obtain the request method by searching for the first space,
    // throw an error if we overun the start line length.
    const size_t req_method_end_pos = request.find(" ");
    if (req_method_end_pos > start_line_length || req_method_end_pos == std::string::npos)
      throw "Unable to find request method";

    // Try to resolve the request method as a JAMP method data-type
    // Clear it after we are done with it.
    std::string req_method = request.substr(0, req_method_end_pos);
    if (req_method == "GET")          method = JAMP::Method::GET;
    else if (req_method == "HEAD")    method = JAMP::Method::HEAD;
    else if (req_method == "POST")    method = JAMP::Method::POST;
    else if (req_method == "PUT")     method = JAMP::Method::PUT;
    else if (req_method == "DELETE")  method = JAMP::Method::DELETE;
    else if (req_method == "CONNECT") method = JAMP::Method::CONNECT;
    else if (req_method == "OPTIONS") method = JAMP::Method::OPTIONS;
    else if (req_method == "TRACE")   method = JAMP::Method::TRACE;
    else throw "Unknown request method";
    req_method.clear();

    // Try to find the end position of the request URI
    // Search for it one character after the request
    // method end position.
    size_t req_uri_end_pos = request.find(SP, req_method_end_pos + SP.size());
    if (req_method_end_pos > start_line_length || req_method_end_pos == std::string::npos)
      throw "Unable to find request URI";

    // Try to resolve the URI into a string
    uri = request.substr(req_method_end_pos + SP.size(), req_uri_end_pos - (req_method_end_pos + SP.size()));

    // Try to find the end position for the HTTP version
    // Search for it using the same method as above.
    size_t req_httpver_end_pos = request.find(CRLF, req_uri_end_pos + 1);
    if (req_method_end_pos > start_line_length || req_method_end_pos == std::string::npos)
      throw "Unable to find request HTTP version";

    // Try to resolve the HTTP version into a string
    http_version = request.substr(req_uri_end_pos + SP.size(), req_httpver_end_pos - (req_uri_end_pos + SP.size()));

  } // End of start line processing


  // Start current position where the first header set would
  // be available.
  size_t current_position = start_line_length + CRLF.size();

  // If we have not reached the end of the header, then lets
  // process request headers.
  while (current_position < header_end)
  {
    // Find the end of this header field and store that position
    size_t req_hdr_field_end = request.find(CRLF, current_position);

    // Find where the header delimiter exists, check if it does not
    // exist, or if it overruns the current header field length.
    size_t req_hdr_field_delimiter_pos = request.find(":", current_position);
    if (req_hdr_field_delimiter_pos == std::string::npos || req_hdr_field_delimiter_pos > req_hdr_field_end)
      throw "Unable to read header field delimiter";

    // Copy the header key into a variable
    std::string key = request.substr(current_position, req_hdr_field_delimiter_pos - current_position);

    // Check if next character is a space, just bump up a position
    // if true.
    if (request[++req_hdr_field_delimiter_pos] == ' ')
      ++req_hdr_field_delimiter_pos;

    std::string value = request.substr(req_hdr_field_delimiter_pos, req_hdr_field_end - req_hdr_field_delimiter_pos);

    fields.emplace(key, value);

    // Set current position to field length + 2 to account for CRLF sequence
    current_position = req_hdr_field_end + CRLF.size();
  }

  // Commit the changes to the object
  m_method = method;
  m_uri = uri;
  m_http_version = http_version;
  m_fields = fields;
}
