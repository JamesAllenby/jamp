#include "URIResolver.hpp"

#include <string>

#include "HTTPCommon.hpp"
#include "HTTPRequest.hpp"

URIResolver::URIResolver()
{
  // Initialise the method map URI Maps
  // located on the heap.
  m_method_map = {
    { JAMP::Method::GET,     new UriMap },
    { JAMP::Method::PUT,     new UriMap },
    { JAMP::Method::HEAD,    new UriMap },
    { JAMP::Method::POST,    new UriMap },
    { JAMP::Method::TRACE,   new UriMap },
    { JAMP::Method::DELETE,  new UriMap },
    { JAMP::Method::CONNECT, new UriMap },
    { JAMP::Method::OPTIONS, new UriMap }
  };
}

// Takes a request URI and resolves it to the most suitable candidate function
HTTPCallback URIResolver::Resolve(JAMP::HTTPRequest request)
{
  // Obtain the relevant URI map by looking at the request method
  auto uri_map = m_method_map.at(request.Method());

  // Check if the URI map is not a null object
  if (uri_map == nullptr)
    throw "URI Map not initialised for this HTTP method";

  // Check if the URI map is not empty either
  if (uri_map->size() < 1)
    throw "URI Map is empty and will not fulfill the request";

  // Loop through the dereferenced uri_map object by key to find a match
  for (auto& uri_map_element : *uri_map)
  {
    if (std::regex_match(request.URI(), std::regex(uri_map_element.first)))
      return uri_map_element.second;
  }

  return [](JAMP::HTTPRequest, JAMP::HTTPResponse response){
    response.SetResponseCode(JAMP::Code::HTTP_404);
    response.Write("Unable to find method to execute");
    response.Close();
  };
}
