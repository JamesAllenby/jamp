#include "JAMP.hpp"

#include <iostream>
#include <functional>
#include <sstream>

#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>

#include "HTTPRequest.hpp"
#include "HTTPResponse.hpp"

JAMP::Server::Server()
{
  // Create a new socket object
  this->m_sock = socket(AF_INET, SOCK_STREAM, 0);
  if (this->m_sock < 0)
  {
    perror("Socket error");
    throw;
  }
}

using namespace JAMP;

void JAMP::Server::All(std::string uri, HTTPCallback callback)
{
  m_uri_resolver.RegisterAll(uri, callback);
}
void JAMP::Server::Get(std::string uri, HTTPCallback callback)
{
  m_uri_resolver.Register<Method::GET>(uri, callback);
}
void JAMP::Server::Put(std::string uri, HTTPCallback callback)
{
  m_uri_resolver.Register<Method::PUT>(uri, callback);
}
void JAMP::Server::Head(std::string uri, HTTPCallback callback)
{
  m_uri_resolver.Register<Method::HEAD>(uri, callback);
}
void JAMP::Server::Post(std::string uri, HTTPCallback callback)
{
  m_uri_resolver.Register<Method::POST>(uri, callback);
}
void JAMP::Server::Trace(std::string uri, HTTPCallback callback)
{
  m_uri_resolver.Register<Method::TRACE>(uri, callback);
}
void JAMP::Server::Delete(std::string uri, HTTPCallback callback)
{
  m_uri_resolver.Register<Method::DELETE>(uri, callback);
}
void JAMP::Server::Connect(std::string uri, HTTPCallback callback)
{
  m_uri_resolver.Register<Method::CONNECT>(uri, callback);
}
void JAMP::Server::Options(std::string uri, HTTPCallback callback)
{
  m_uri_resolver.Register<Method::OPTIONS>(uri, callback);
}

[[noreturn]] void JAMP::Server::Start(ushort port)
{
  sockaddr_in server_address = {};
  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr = INADDR_ANY;
  server_address.sin_port = htons(port);

  if (bind(this->m_sock, reinterpret_cast<sockaddr*>(&server_address), sizeof(server_address)) < 0)
  {
    perror("Bind error");
    throw;
  }

  if (listen(this->m_sock, SOMAXCONN))
  {
    perror("Listen error");
    throw;
  }

  sockaddr_in client_address = {};
  socklen_t client_length = {};

  while (true)
  {
    int connection_fd = accept(m_sock, reinterpret_cast<sockaddr*>(&client_address), &client_length);
    if (connection_fd < 0)
    {
      perror("Connection error");
      continue;
    }

    char message[MAX_CLIENT_MESSAGE] = {};
    long message_length = read(connection_fd, &message, MAX_CLIENT_MESSAGE);
    std::string client_message(message, static_cast<size_t>(message_length));

    JAMP::HTTPRequest request(client_message);
    JAMP::HTTPResponse response(connection_fd);

    m_uri_resolver.Resolve(request)(request, response);

  }
}
