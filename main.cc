#include "JAMP.hpp"

int main()
{
  // Create a JAMP server object, the constructor is called for us automatically!
  JAMP::Server server;

  // Register a URI path for the root to catch all types of requests.
  server.All("/", [](auto&, auto& response)
  {
    response.SetResponseCode(JAMP::Code::HTTP_200);
    response.Write("Hey there! Welcome to JAMP.");
    response.Close();
  });

  // Register absolute URIs for particular paths
  server.All("/rules", [](auto&, auto& response)
  {
    response.SetResponseCode(JAMP::Code::HTTP_200);
    response.Write("These are the rules... don't break them.");
    response.Close();
  });

  // Match all URLs that contain /profile/ and anything after
  // This will also ensure that it only matches GET requests.
  server.Get("/profile/[a-zA-Z]+", [](auto& request, auto& response)
  {
    response.SetResponseCode(JAMP::Code::HTTP_200);
    response.Write("Welcome to the profile section of the website\n\n");
    response.Write(request.URI());
    response.Close();
  });

  // Match all URLs that contain /profile/ and anything after
  // This will also ensure that it only matches GET requests.
  server.Get("/profile/[a-zA-Z]+/edit", [](auto& request, auto& response)
  {
    response.SetResponseCode(JAMP::Code::HTTP_200);
    response.Write("Welcome to the profile section of the website\n\n");
    response.Write(request.URI());
    response.Close();
  });

  // Register paths for executing special functionality
  server.All("/quit", [](auto&, auto& response)
  {
    response.SetResponseCode(JAMP::Code::HTTP_200);
    response.Write("Shutting down server...");
    response.Close();
    std::exit(EXIT_SUCCESS);
  });

  // Once you're done registering your paths, finally you can run your server!
  server.Start(10001);
}
