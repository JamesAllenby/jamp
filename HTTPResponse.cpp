#include "HTTPResponse.hpp"

#include <string>
#include <sstream>
#include <algorithm>

#include <unistd.h>

// Construct a response object with a connection file descriptor
JAMP::HTTPResponse::HTTPResponse(int connection_fd) : m_connection_fd(connection_fd) {}

// Set the response code of the response
void JAMP::HTTPResponse::SetResponseCode(JAMP::Code response_code)
{
  m_response_code = response_code;
}

// Append a header to the response
void JAMP::HTTPResponse::AddHeader(const std::string& key, const std::string& value)
{
  m_fields.emplace(key, value);
}

void JAMP::HTTPResponse::Write(std::string text)
{
  m_message += text;
}

const std::string JAMP::HTTPResponse::Serialize()
{
  std::stringstream response_stream;

  // Write the start line
  response_stream << "HTTP/1.1 " << JAMP::StringCode.at(m_response_code) + " " << JAMP::ReasonPhrase.at(m_response_code) + CRLF;

  // Write the headers
  std::for_each(m_fields.begin(), m_fields.end(), [&response_stream](auto field){
    response_stream << field.first << ": " << field.second << CRLF;
  });

  // End the header and append the message
  response_stream << CRLF << m_message;

  return response_stream.str();
}

// Write to file descriptor and close server
void JAMP::HTTPResponse::Close()
{
  std::string response = Serialize();
  write(this->m_connection_fd, response.c_str(), response.size());
  close(m_connection_fd);
  return;
}
